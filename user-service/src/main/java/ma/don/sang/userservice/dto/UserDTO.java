package ma.don.sang.userservice.dto;

import lombok.Data;

@Data
public class UserDTO {

    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String street;
    private String streetNumber;
    private String zipCode;
    private String locality;
    private String country;
    private String sangType;
    private String userName;
    private String userPassword;
    private String captcha;

}
