package ma.don.sang.userservice.exception;


public class TechnicalException extends RuntimeException {

  private static final long serialVersionUID = -3372433568932641320L;

  public TechnicalException(final String message) {
    super(message);
  }

  public TechnicalException(final String message, final Throwable e) {
    super(message, e);
  }



}
