package ma.don.sang.userservice.exception;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Error {

  private Integer code;
  private String message;


}
