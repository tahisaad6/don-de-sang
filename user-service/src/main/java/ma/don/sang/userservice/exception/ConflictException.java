package ma.don.sang.userservice.exception;


public class ConflictException extends RuntimeException {

	private static final long serialVersionUID = -3372433568932641320L;

	public ConflictException(final String message) {
		super(message);
	}

	public ConflictException(final String message, final Throwable e) {
		super(message, e);
	}
}
