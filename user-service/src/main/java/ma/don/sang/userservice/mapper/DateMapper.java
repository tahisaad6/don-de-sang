package ma.don.sang.userservice.mapper;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class DateMapper {
	private final DateFormat ft = new SimpleDateFormat("hh:mm a");

	public Date asDate(String date) throws Throwable {
		return date != null ? ft.parse(date) : null;
	}

	public String asString(Date date) throws Throwable {
		return date != null ? ft.format(date) : null;
	}
}
