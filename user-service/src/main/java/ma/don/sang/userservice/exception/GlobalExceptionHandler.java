package ma.don.sang.userservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = { TechnicalException.class })
	protected ResponseEntity<Error> handleTechnicalException(RuntimeException ex, WebRequest request) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(Error.builder().code(500).message(ex.getMessage()).build());

	}

	@ExceptionHandler(value = { NotFoundException.class })
	protected ResponseEntity<Error> handleNotFoundException(RuntimeException ex, WebRequest request) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(Error.builder().code(404).message(ex.getMessage()).build());

	}

	@ExceptionHandler(value = { IllegalArgumentException.class })
	protected ResponseEntity<Error> handleIllegalArgumentException(RuntimeException ex, WebRequest request) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(Error.builder().code(HttpStatus.BAD_REQUEST.value()).message(ex.getMessage()).build());

	}

	@ExceptionHandler(value = { ConflictException.class })
	protected ResponseEntity<Error> handleConflictException(RuntimeException ex, WebRequest request) {
		return ResponseEntity.status(HttpStatus.CONFLICT)
				.body(Error.builder().code(409).message(ex.getMessage()).build());

	}

	@ExceptionHandler(value = { WrongPasswordException.class })
	protected ResponseEntity<Error> handleWrongPasswordException(RuntimeException ex, WebRequest request) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(Error.builder().code(403).message(ex.getMessage()).build());

	}

}
