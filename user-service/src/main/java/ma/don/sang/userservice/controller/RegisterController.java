package ma.don.sang.userservice.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ma.don.sang.userservice.dto.UserDTO;
import ma.don.sang.userservice.entity.User;
import ma.don.sang.userservice.exception.TechnicalException;
import ma.don.sang.userservice.service.RecaptchaService;
import ma.don.sang.userservice.service.UserService;

@RestController
public class RegisterController {

	@Autowired
	private UserService userService;

	@Autowired
	private RecaptchaService recaptchaService;
	
	@Value("${hasCaptcha}")
	private Boolean canValidateCaptcha;

	@PostMapping(value = "/registration")
	public ResponseEntity<?> addUser(@RequestBody UserDTO userDTO, HttpServletRequest request) {

		if (userDTO == null || userDTO != null && !recaptchaService.verifyRecaptcha(userDTO.getCaptcha(), request.getRemoteAddr()) && canValidateCaptcha ) {
			throw new IllegalArgumentException("Captcha incorrect");
		}

		try {
			userService.saveUser(userDTO);
			return new ResponseEntity<User>(HttpStatus.CREATED);
		} catch (Exception e) {
			throw new TechnicalException("Creation error.");
		}
	}
}
