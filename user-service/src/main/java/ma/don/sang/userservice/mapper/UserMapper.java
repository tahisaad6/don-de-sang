package ma.don.sang.userservice.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import ma.don.sang.userservice.dto.UserDTO;
import ma.don.sang.userservice.entity.User;

@Mapper(config = MapStructConfig.class,unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {

	User mapToEntity(UserDTO userDTO);

	UserDTO mapToDTO(User user);

	List<User> mapToListOfEntity(List<UserDTO> agenceDTO);

	List<UserDTO> mapToListOfDTO(List<User> agence);

}
