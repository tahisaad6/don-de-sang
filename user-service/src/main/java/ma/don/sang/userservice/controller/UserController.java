package ma.don.sang.userservice.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ma.don.sang.userservice.dto.UserDTO;
import ma.don.sang.userservice.exception.IllegalArgumentException;
import ma.don.sang.userservice.exception.NotFoundException;
import ma.don.sang.userservice.exception.TechnicalException;
import ma.don.sang.userservice.service.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;


	@GetMapping(value = "/users")
	public ResponseEntity<List<UserDTO>> getAllUsers() {
		return new ResponseEntity<List<UserDTO>>(userService.getAllUsers(), HttpStatus.OK);
	}

	@GetMapping(value = "/users", params = "name")
	public ResponseEntity<UserDTO> getUserByName(@RequestParam("name") String userName) {
		UserDTO user = userService.getUserByName(userName);
		if (user != null) {
			return new ResponseEntity<UserDTO>(user,HttpStatus.OK);
		}
		throw new NotFoundException("User introuvable");
	}

	@GetMapping(value = "/users/{id}")
	public ResponseEntity<UserDTO> getUserById(@PathVariable("id") Long id) {
		UserDTO user = userService.getUserById(id);
		if (user != null) {
			return new ResponseEntity<UserDTO>(user, HttpStatus.OK);
		}
		throw new NotFoundException("");
	}

	@PostMapping(value = "/users")
	public ResponseEntity<UserDTO> addUser(@RequestBody UserDTO userDTO, HttpServletRequest request) {
		if (userDTO != null) {
			throw new IllegalArgumentException("User can't be null");
		}
		try {
			userService.saveUser(userDTO);
			return new ResponseEntity<UserDTO>(HttpStatus.CREATED);
		} catch (Exception e) {
			throw new TechnicalException("Creation error.");
		}

	}
}
