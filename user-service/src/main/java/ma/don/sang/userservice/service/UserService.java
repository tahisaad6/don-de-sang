package ma.don.sang.userservice.service;
import java.util.List;

import ma.don.sang.userservice.dto.UserDTO;

public interface UserService {
    List<UserDTO> getAllUsers();
    UserDTO getUserById(Long id);
    UserDTO getUserByName(String userName);
    UserDTO saveUser(UserDTO user);
}
