package ma.don.sang.userservice.exception;


public class IllegalArgumentException extends RuntimeException {

  private static final long serialVersionUID = -3372433568932641320L;


  public IllegalArgumentException(final String message, final Throwable e) {
    super(message, e);
  }


  public IllegalArgumentException(final String message) {
    super(message);
  }

}
