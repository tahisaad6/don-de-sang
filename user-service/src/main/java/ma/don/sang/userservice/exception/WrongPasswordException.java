package ma.don.sang.userservice.exception;

public class WrongPasswordException extends RuntimeException {

	  private static final long serialVersionUID = -337243568932641320L;


	  public WrongPasswordException(final String message, final Throwable e) {
	    super(message, e);
	  }


	  public WrongPasswordException(final String message) {
	    super(message);
	  }

	}
