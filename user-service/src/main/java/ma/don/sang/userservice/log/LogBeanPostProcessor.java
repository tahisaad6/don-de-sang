package ma.don.sang.userservice.log;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.NameMatchMethodPointcut;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;


@Component
public class LogBeanPostProcessor implements BeanPostProcessor, Ordered {

    private Logger log = LoggerFactory.getLogger(LogBeanPostProcessor.class);

    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }

    @Override
    public Object postProcessBeforeInitialization(final Object bean, final String beanName) {
        final List<String> methodNames = new ArrayList<>();
        ReflectionUtils.doWithMethods(bean.getClass(), method -> {
            if (method.isAnnotationPresent(LogArgumentsAndResult.class)) {
                methodNames.add(method.getName());
            }
        });

        return methodNames.isEmpty() ? bean : createProxy(bean, methodNames);
    }

    @Override
    public Object postProcessAfterInitialization(final Object bean, final String beanName) {
        return bean;
    }

    private Object createProxy(Object target, List<String> methodNames) {
        final NameMatchMethodPointcut pointcut = new NameMatchMethodPointcut();
        pointcut.setMappedNames(methodNames.toArray(new String[methodNames.size()]));

        final ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.addAdvisor(new DefaultPointcutAdvisor(pointcut, new LogMethodInterceptor()));
        proxyFactory.setTarget(target);
        return proxyFactory.getProxy();
    }

    private class LogMethodInterceptor implements MethodInterceptor {

        @Override
        public Object invoke(MethodInvocation methodInvocation) throws Throwable {
            final Object target = methodInvocation.getThis();
            String className = target.getClass().getSimpleName();
            Method method = methodInvocation.getMethod();
            String methodName = method.getName();
            final Object[] arguments = methodInvocation.getArguments();
            final Monitor monitor = MonitorFactory.start(className + "." + methodName);

            final StringBuilder parametersStringBuilder = new StringBuilder();
            parametersStringBuilder.append(className)
                    .append('.')
                    .append(methodName)
                    .append(" : ");
            final Object[] args = methodInvocation.getArguments();
            final Parameter[] parameters = method.getParameters();
            for (int i = 0; i < args.length; i++) {
                parametersStringBuilder.append(parameters[i].getName())
                        .append(" = ")
                        .append(args[i])
                        .append(i == args.length - 1 ? "" : ", ");
            }
            log.info(parametersStringBuilder.toString());

            Object result;
            try {
                result = method.invoke(target, arguments);
                log.info("{}.{} : result {}", className, methodName, method.getReturnType().equals(Void.TYPE) ? "void" : "= " + result);
            } catch (Exception e) {
                log.info(e.getMessage());
                throw e.getCause() != null ? e.getCause() : e;
            } finally {
                monitor.stop();
            }
            return result;
        }
    }
}