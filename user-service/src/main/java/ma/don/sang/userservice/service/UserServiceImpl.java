package ma.don.sang.userservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ma.don.sang.userservice.dto.UserDTO;
import ma.don.sang.userservice.entity.User;
import ma.don.sang.userservice.entity.UserRole;
import ma.don.sang.userservice.mapper.UserMapper;
import ma.don.sang.userservice.repository.UserRepository;
import ma.don.sang.userservice.repository.UserRoleRepository;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserRoleRepository userRoleRepository;

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public List<UserDTO> getAllUsers() {
		return userMapper.mapToListOfDTO(userRepository.findAll());
	}

	@Override
	public UserDTO getUserById(Long id) {
		return userMapper.mapToDTO(userRepository.getOne(id));
	}

	@Override
	public UserDTO getUserByName(String userName) {
		return userMapper.mapToDTO(userRepository.findByUserName(userName));
	}

	@Override
	public UserDTO saveUser(UserDTO userDTO) {
		User user = userMapper.mapToEntity(userDTO);
		user.setActive(1);
		UserRole role = userRoleRepository.findUserRoleByRoleName("ROLE_USER");
		user.setUserPassword(passwordEncoder.encode(userDTO.getUserPassword()));
		user.setRole(role);
		return userMapper.mapToDTO(userRepository.save(user));
	}
}
